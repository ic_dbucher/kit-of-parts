import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { PlatformModule } from './platform/platform.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpClientModule, PlatformModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
