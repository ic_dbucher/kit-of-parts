import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { ViewerModule } from '../viewer/viewer.module';

import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    ViewerModule,
    ButtonModule,
    ConfirmDialogModule,
  ],
  exports: [MainComponent],
  providers: [ConfirmationService]
})
export class PlatformModule { }
