export class Part {
  public internalID: number;
  public parentID: number;
  public parentName: string;
  public typeName: string;
  public structuralMaterial: string;
  public category: number;
  public subCategory: number;
  public length: number;
  public sequenzing: number;
  public relations: number[];
  // [x_max, y_max, z_max, x_min, y_min, z_min] 
  public boundingBox: number[];

  constructor(internalID: number, parentID: number, parentName: string, typeName: string, structuralMaterial: string, category: number, subCategory: number, length: number, sequenzing: number, relations: number[], boundingBox: number[]) {
    this.internalID = internalID;
    this.parentID = parentID;
    this.parentName = parentName;
    this.typeName = typeName;
    this.structuralMaterial = structuralMaterial;
    this.category = category;
    this.subCategory = subCategory;
    this.length = length;
    this.sequenzing = sequenzing;
    this.relations = relations;
    this.boundingBox = boundingBox;
  }
}