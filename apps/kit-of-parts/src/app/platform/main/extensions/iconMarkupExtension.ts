/// <reference types="forge-viewer" />

import { Extension } from '../../../viewer/extensions/extension';

declare const THREE: any;

// Funktionen für asynchrones forEach
const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

const searchPromise = (that, args) => {
    return new Promise((resolve, reject) => {
        that.viewer.search(args, (idArray) => {
            resolve(idArray);
        }, (err) => {
            reject(false);
        }, ['name']
        );
    });
};


export class IconMarkupExtension extends Extension {
    // Extension must have a name
    public static extensionName: string = 'IconMarkupExtension';
    public _group;
    public _button: Autodesk.Viewing.UI.Button;
    public _icons;
    public _inputs;
    public _enabled;
    public _frags;
    public onClick;
    public clickedParameter;

    constructor(viewer, options) {
        super(viewer, options);
        this._group = null;
        this._button = null;
        this._enabled = false;
        this._icons = [];
        this._inputs = options.inputs || [];
        this.clickedParameter = options.clickedParameter;
        this.onClick = (id) => {
            this.viewer.select(id);
            this.viewer.fitToView([id], this.viewer.model, false);
        };
    }

    public async load() {
        this._enabled = true;
        // const updateIconsCallback = () => {
        //     if (this._enabled) {
        //         this.updateIcons();
        //     }
        // };
        // this.viewer.addEventListener(Autodesk.Viewing.CAMERA_CHANGE_EVENT, updateIconsCallback);
        // this.viewer.addEventListener(Autodesk.Viewing.ISOLATE_EVENT, updateIconsCallback);
        // this.viewer.addEventListener(Autodesk.Viewing.HIDE_EVENT, updateIconsCallback);
        // this.viewer.addEventListener(Autodesk.Viewing.SHOW_EVENT, updateIconsCallback);

        // // Iterate through _inputs
        // await asyncForEach(this._inputs, async (input) => {
        //     // this._inputs.forEach(input => {
        //     // this._inputs.forEach(input => {
        //     var name = '';
        //     if (input.objectPath.indexOf('/')) {
        //         name = input.objectPath.split('/')[input.objectPath.split('/').length - 1];
        //     }
        //     else {
        //         name = input.objectPath;
        //     }
        //     await searchPromise(this, name).then(id => {
        //         // Abgreifen wenn der Input zwar in Datenbank aber nicht im Modell => id[0] = undefined
        //         if(id[0]){
        //         this._icons.push({ dbId: id[0], label: input[this.clickedParameter], css: 'fas fa-question-circle', textcss: 'font-size: 100px' });
        //         }
        //     });
        // });
        // this.showIcons(true);
        return true;
    }

    unload() {
        this._enabled = false;
        this.showIcons(false);
        // Clean our UI elements if we added any
        if (this._group) {
            this._group.removeControl(this._button);
            if (this._group.getNumberOfControls() === 0) {
                this.viewer.toolbar.removeControl(this._group);
            }
        }
        return true;
    }

    drawBox(min, max) {
        const linesMaterial = new THREE.LineBasicMaterial({
            color: new THREE.Color(0xFF0000),
            transparent: true,
            depthWrite: false,
            depthTest: true,
            linewidth: 10,
            opacity: 1.0
        });
        this.drawLines([

            { x: min.x, y: min.y, z: min.z },
            { x: max.x, y: min.y, z: min.z },

            { x: max.x, y: min.y, z: min.z },
            { x: max.x, y: min.y, z: max.z },

            { x: max.x, y: min.y, z: max.z },
            { x: min.x, y: min.y, z: max.z },

            { x: min.x, y: min.y, z: max.z },
            { x: min.x, y: min.y, z: min.z },

            { x: min.x, y: max.y, z: max.z },
            { x: max.x, y: max.y, z: max.z },

            { x: max.x, y: max.y, z: max.z },
            { x: max.x, y: max.y, z: min.z },

            { x: max.x, y: max.y, z: min.z },
            { x: min.x, y: max.y, z: min.z },

            { x: min.x, y: max.y, z: min.z },
            { x: min.x, y: max.y, z: max.z },

            { x: min.x, y: min.y, z: min.z },
            { x: min.x, y: max.y, z: min.z },

            { x: max.x, y: min.y, z: min.z },
            { x: max.x, y: max.y, z: min.z },

            { x: max.x, y: min.y, z: max.z },
            { x: max.x, y: max.y, z: max.z },

            { x: min.x, y: min.y, z: max.z },
            { x: min.x, y: max.y, z: max.z }],

            linesMaterial);
    }

    drawLines(coordsArray, material) {
        for (let i = 0; i < coordsArray.length; i += 2) {
            const start = coordsArray[i];
            const end = coordsArray[i + 1];
            const geometry = new THREE.Geometry();
            geometry.vertices.push(new THREE.Vector3(start.x, start.y, start.z));
            geometry.vertices.push(new THREE.Vector3(end.x, end.y, end.z));
            geometry.computeLineDistances();
            const lines = new THREE.Line(geometry,
                material,
                THREE.LinePieces);
            // @ts-ignore
            if (!this.viewer.overlays.hasScene('bounding-box')) {
                // @ts-ignore
                this.viewer.overlays.addScene('bounding-box');
            }
            // @ts-ignore
            this.viewer.overlays.addMesh(lines, 'bounding-box');
        }
    }

    // async onToolbarCreated() {
    //     // Create a new toolbar group if it doesn't exist
    //     this._group = this.viewer.toolbar.getControl('customExtensions');
    //     if (!this._group) {
    //         this._group = new Autodesk.Viewing.UI.ControlGroup('customExtensions');
    //         this.viewer.toolbar.addControl(this._group);
    //     }

    //     // Add a new button to the toolbar group
    //     this._button = new Autodesk.Viewing.UI.Button('IconExtension');
    //     this._button.onClick = (ev) => {
    //         this._enabled = !this._enabled;
    //         this.showIcons(this._enabled);
    //         this._button.setState(this._enabled ? 0 : 1);
    //     };

    //     // this._button.setToolTip(this.options.button.tooltip);
    //     this._button.setToolTip('Showing Panel Information');
    //     // @ts-ignore
    //     this._button.container.children[0].classList.add('fas', 'fa-cogs');
    //     this._group.addControl(this._button);

    //     // Iterate through _inputs
    //     await asyncForEach(this._inputs, async (input) => {
    //         // this._inputs.forEach(input => {
    //         // this._inputs.forEach(input => {
    //         var name = '';
    //         if (input.objectPath.indexOf('/')) {
    //             name = input.objectPath.split('/')[input.objectPath.split('/').length - 1];
    //         }
    //         else {
    //             name = input.objectPath;
    //         }
    //         await this.viewer.search(name, (idArray) => {
    //             if (idArray.length === 1) {
    //                 console.log('idArray');
    //                 this._icons.push({ dbId: idArray[0], label: name, css: 'fas fa-question-circle', textcss: 'font-size: 100px' });
    //             }
    //             else if (idArray.length !== 1) {
    //                 console.log('idArray.length !== 1 getMarkups!!');
    //             }
    //         }, (err) => {
    //             console.log('Something with GETTING MARKUPS went wrong');
    //         }, ['name']);
    //     });
    // }

    showIcons(show) {
        // console.log(this.viewer);
        // console.log(this._icons);
        // @ts-ignore
        const $viewer = $('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer');

        // remove previous...
        // @ts-ignore
        $('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer label.markup').remove();

        // console.log(show);

        if (!show) return;

        // do we have anything to show?
        if (this._icons === undefined || this._icons === null) return;

        // do we have access to the instance tree?
        const tree = this.viewer.model.getInstanceTree();

        // console.log(tree);

        if (tree === undefined) { console.log('Loading tree...'); return; }

        const onClick = (e) => {
            this.onClick($(e.currentTarget).data('id'));
        };

        this._frags = {};

        // console.log(this._icons);
        // console.log(this._icons.length);


        this._icons.forEach(iconTemp => {
            // for (var i = 0; i < this._icons.length; i++) {
            // console.log(i);
            // we need to collect all the fragIds for a given dbId
            // const icon = this._icons[i];
            const icon = iconTemp;
            this._frags['dbId' + icon.dbId] = [];

            // create the label for the dbId
            // const $label = $(`
            // <label class="markup update" data-id="${icon.dbId}">
            //     <span class="${icon.css}" style="color: rgb(113, 141, 221)"></span>
            //     <span style="color: rgb(113, 141, 221)" class="${icon.textcss}"> ${icon.label || ''}</span>
            // </label>
            // `);
            const $label = $(`
            <label class="markup update" data-id="${icon.dbId}">
                <span style="color: rgb(113, 141, 221)" class="${icon.textcss}"> ${icon.label || ''}</span>
            </label>
            `);
            $label.css('display', this.viewer.isNodeVisible(icon.dbId) ? 'block' : 'none');
            $label.on('click', onClick);

            $viewer.append($label);

            // console.log(icon);
            // console.log(this._frags);

            // now collect the fragIds
            const _this = this;
            tree.enumNodeFragments(icon.dbId, (fragId) => {
                _this._frags['dbId' + icon.dbId].push(fragId);
                _this.updateIcons(); // re-position of each fragId found
            });
        });
    }

    getModifiedWorldBoundingBox(dbId) {
        var fragList = this.viewer.model.getFragmentList();
        const nodebBox = new THREE.Box3();

        // for each fragId on the list, get the bounding box
        for (const fragId of this._frags['dbId' + dbId]) {
            const fragbBox = new THREE.Box3();
            // @ts-ignore
            fragList.getWorldBounds(fragId, fragbBox);
            nodebBox.union(fragbBox); // create a unifed bounding box
        }

        return nodebBox;
    }

    updateIcons() {
        // @ts-ignore
        // const label = $('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer .update')[0];
        // const $label = $(label);
        // @ts-ignore
        // @ts-ignore
        $(() => {
            // @ts-ignore
            const labels = Array.from($('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer .update'));
            for (const label of labels) {
                const $label = $(label);
                const id = $label.data('id');
                // get the center of the dbId(based on its fragIds bounding boxes)
                const pos = this.viewer.worldToClient(this.getModifiedWorldBoundingBox(id).center());
                // position the label center to it
                $label.css('left', Math.floor(pos.x - $label[0].offsetWidth / 2) + 'px');
                $label.css('top', Math.floor(pos.y - $label[0].offsetHeight / 2) + 'px');
                $label.css('display', this.viewer.isNodeVisible(id) ? 'block' : 'none');
            }
        });

        // for (const label of $('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer .update')) {
        //     const $label = $(label);
        //     const id = $label.data('id');

        //     // @ts-ignore
        //     // const $label = $('#' + this.viewer.clientContainer.id + ' div.adsk-viewing-viewer .update')[0];
        //     // const id = $label.data('id');

        //     // get the center of the dbId (based on its fragIds bounding boxes)
        //     const pos = this.viewer.worldToClient(this.getModifiedWorldBoundingBox(id).center());
        //     // console.log(pos);

        //     // position the label center to it
        //     $label.css('left', Math.floor(pos.x - $label[0].offsetWidth / 2) + 'px');
        //     $label.css('top', Math.floor(pos.y - $label[0].offsetHeight / 2) + 'px');
        //     $label.css('display', this.viewer.isNodeVisible(id) ? 'block' : 'none');
        // }
    }
}
