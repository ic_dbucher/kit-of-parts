import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

import {
  ViewerOptions,
  ViewerInitializedEvent,
  DocumentChangedEvent,
  ViewerComponent,
} from '../../viewer/component/viewer.component';

import {
  SelectionChangedEventArgs,
  // ExtensionLoadedEventArgs, Mal ausprobiert mit dem ExtensionLoaded Event
  Extension,
  ObjectTreeCreatedEventArgs,
  IsolateEventArgs,
} from '../../viewer/extensions/extension';

import { Part } from '../models/part';

import { AuthToken } from 'forge-apis';
import { ApiService } from '../../_services/api.service';

import * as $ from 'jquery';
import { AppComponent } from '../../app.component';

declare var THREE: any;

import { IconMarkupExtension } from './extensions/iconMarkupExtension';

// import html from './legendTemplate.html';

// Function for async forEach
const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  @Input() app: AppComponent;

  public viewerOptions3d: ViewerOptions;
  public encodedmodelurn: string;

  // Graphical Stuff
  public toolbarSelection: Autodesk.Viewing.UI.ToolBar;
  public toolbarTest: Autodesk.Viewing.UI.ToolBar;
  public button1: Autodesk.Viewing.UI.Button;
  public button2: Autodesk.Viewing.UI.Button;

  public selectedParts: Part[] = new Array();

  // Model stuff
  public parts: Part[] = new Array();
  public relations: number[][] = new Array();
  public allTypesValue: string[];

  // Autodesk stuff
  public buckets = new Array();
  public objectsPerBucket = new Array();


  public cbCount: number;
  public viewerLeafNodes: number[];

  @ViewChild(ViewerComponent, { static: false })
  viewerComponent: ViewerComponent;

  constructor(private api: ApiService, private confirmationService: ConfirmationService) {

    // this.api.getspecificProject('5faa62b2079c07001454c421').then((res) => {
    //   this.encodedmodelurn = res.encodedmodelurn;
    // });
    // this.encodedmodelurn = 'dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDIxLTAxLTIzLTE0LTQ0LTIxLWQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0MjdlL0EyMjUucnZ0';
    // this.encodedmodelurn =
    //   'dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6bW9kZWwyMDIxLTAxLTIzLTE0LTQ0LTIxLWQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0MjdlL0EyMjUucnZ0';
    this.viewerOptions3d = {
      initializerOptions: {
        env: 'AutodeskProduction',
        getAccessToken: async (onGetAccessToken) => {
          const authToken: AuthToken = await this.api
            .get2LToken()
            .then((res) => {
              return res.access_token;
            });
          onGetAccessToken(authToken, 30 * 60);
        },
        api: 'derivativeV2',
      },
      viewerConfig: {
        // IconMarkupExtension wird bei onViewerInitialized geladen
        extensions: [], //['IconMarkupExtension'], //'Autodesk.Snapping',
        theme: 'dark-theme',
      },
      onViewerScriptsLoaded: this.scriptsLoaded,
      onViewerInitialized: async (args: ViewerInitializedEvent) => {
        // For starting take the first bucket and first object
        // First Bucket
        // 'model2021-01-23-14-44-21-d41d8cd98f00b204e9800998ecf8427e'
        // First Bucket
        // 'model2021-01-26-08-46-09-d41d8cd98f00b204e9800998ecf8427e'
        await this.api.getObjects('model2021-01-26-08-46-09-d41d8cd98f00b204e9800998ecf8427e').then(ress => {
          this.encodedmodelurn = btoa(ress.body.items[0].objectId)
        });
        if (this.encodedmodelurn) {
          args.viewerComponent.DocumentId = this.encodedmodelurn;
        }
        // Hide container where model is in
        this.replaceSpinner();
        // $('.lds-roller').show();
        $('.lds-roller').addClass('importantRuleDisplay');
        $('canvas').hide();

        this.loadSelectionToolbar();
        this.loadHorizontalToolbar();

        await this.api.getBuckets().then(async (res) => {
          this.buckets = res.body.items;
          asyncForEach(this.buckets, async (item) => {
            await this.api.getObjects(item.bucketKey).then(ress => {
              this.objectsPerBucket = ress.body.items;
              console.log(this.objectsPerBucket);
              console.log(this.buckets);
            });
          });
        });

        this.viewerComponent.viewer.setGhosting(false);
        await this.api.emptydatabase().then(res => {
          if (res) {
            console.log('Database has been deleted');
          }
        })
        // Crapy Solution
        // Store all Leaf Nodes to this.viewerLeafNodes
        this.storeAllLeafNodes();
        // console.log('this.viewerLeafNodes');
        // console.log(this.viewerLeafNodes);

        setTimeout(() => {
          this.storeAllParts();
        }, 5000);
        setTimeout(() => {
          this.calculateNeighbors();
          clearInterval();
        }, 10000);
        setTimeout(() => {
          this.api.createMultipleParts(this.parts).then(() => {
            this.api.createRelations(this.relations).then(res => {
              $('.lds-roller').removeClass('importantRuleDisplay');
              $('canvas').show();
              this.allTypesValue = [...new Set(this.parts.map(part => part.typeName))];
            });
          });
        }, 15000);
      },
      // Muss true sein
      showFirstViewable: true,
      // Ist falsch gesetzt => GuiViewer3D => Buttons ausgeblendet in Viewer CSS
      headlessViewer: false,
    };
  }

  ngOnInit(): void {
    $('.lds-roller').show();
  }

  public testing() {
    console.log(this.parts);
    console.log(this.allTypesValue);
    const categoryOne = this.parts.filter(x => x.typeName === this.allTypesValue[0]);
    const categroyOneAllLength = [...new Set(categoryOne.map(part => part.length))];
    console.log(categoryOne);
    console.log(categroyOneAllLength);
  }

  public async scriptsLoaded() {
    // Extension.registerExtension('IconMarkupExtension', IconMarkupExtension);
  }

  public replaceSpinner() {
    const spinners = document.getElementsByClassName('forge-spinner');
    if (spinners.length === 0) {
      return;
    }
    const spinner = spinners[0];
    spinner.classList.remove('forge-spinner');
    spinner.classList.add('lds-roller');
    spinner.innerHTML =
      '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';
  }

  public loadSelectionToolbar() {
    // Button Start Sequenzing
    const button1 = new Autodesk.Viewing.UI.Button('start-selection');
    this.button1 = button1;
    button1.addClass('start-selection');
    button1.setToolTip('Start Sequenzing');
    // @ts-ignore
    button1.container.children[0].classList.add('fas', 'fa-hand-pointer');

    // Button Start Getting Neighbors
    const button2 = new Autodesk.Viewing.UI.Button('get-neighbors');
    this.button2 = button2;
    button2.addClass('get-neighbors');
    button2.setToolTip('Get Neighbors');
    // @ts-ignore
    button2.container.children[0].classList.add('fas', 'fa-user-minus');

    // SubToolbar
    const controlGroup = new Autodesk.Viewing.UI.ControlGroup(
      'my-custom-controlGroup-vertical'
    );
    controlGroup.addControl(button1);
    controlGroup.addControl(button2);
    // Toolbar
    this.toolbarSelection = new Autodesk.Viewing.UI.ToolBar(
      'my-custom-toolbar-vertical',
      { collapsible: false, alignVertically: true }
    );
    button1.onClick = (event) => {
      if (button1.getState() === 1) {
        button1.setState(0)
      } else {
        this.openOverlay();
      }
    };
    button2.onClick = (event) => {
      if (button2.getState() === 1) {
        button2.setState(0)
      } else {
        button2.setState(1)
      }
    };
    this.toolbarSelection.addControl(controlGroup);
    $(this.viewerComponent.viewer.container).append(
      this.toolbarSelection.container
    );
  }

  public loadHorizontalToolbar() {
    //button test
    const button1 = new Autodesk.Viewing.UI.Button('testing');
    button1.addClass('testing');
    button1.setToolTip('testing');
    //@ts-ignore
    button1.container.children[0].classList.add('fas', 'fa-comment-dots');
    button1.onClick = (event) => {
      this.testing();
    };

    // const button2 = new Autodesk.Viewing.UI.Button('showing-WDreinforcement');
    // button2.addClass('showing-WDreinforcement');
    // //@ts-ignore
    // button2.container.children[0].classList.add('far', 'fa-registered');

    // const button3 = new Autodesk.Viewing.UI.Button('showing-WDconcrete');
    // button3.addClass('showing-WDconcrete');
    // //@ts-ignore
    // button3.container.children[0].classList.add('fas', 'fa-truck-pickup');

    // const button4 = new Autodesk.Viewing.UI.Button('showing-WDcuring');
    // button4.addClass('showing-WDcuring');
    // //@ts-ignore
    // button4.container.children[0].classList.add('fab', 'fa-cuttlefish');

    // const button5 = new Autodesk.Viewing.UI.Button('showing-WDstrip');
    // button5.addClass('showing-WDstrip');
    // //@ts-ignore
    // button5.container.children[0].classList.add('fab', 'fa-stripe-s');

    const controlGroup = new Autodesk.Viewing.UI.ControlGroup(
      'my-custom-controlGroup-horizontal'
    );
    controlGroup.addControl(button1);
    // controlGroup.addControl(button2);
    // controlGroup.addControl(button3);
    // controlGroup.addControl(button4);
    // controlGroup.addControl(button5);
    // Toolbar
    this.toolbarTest = new Autodesk.Viewing.UI.ToolBar(
      'my-custom-toolbar-horizontal',
      { collapsible: true, alignVertically: false }
    );

    // There we have to wait since the toolbar is not loaded
    this.toolbarTest.addControl(controlGroup);
    $(this.viewerComponent.viewer.container).append(
      this.toolbarTest.container
    );
  }

  onReject() {
    this.closeOverlay();
  }

  async onConfirm() {
    this.button1.setState(1);
    this.closeOverlay();
    this.selectedParts = new Array();
  }

  public openOverlay() {
    document.getElementById('overlay').style.height = '100%';
  }

  public closeOverlay() {
    document.getElementById('overlay').style.height = '0%';
  }

  public async getBulkProperties(ids: number[], propFilter: string[]) {
    return new Promise((resolve, rejected) => {
      this.viewerComponent.viewer.model.getBulkProperties(
        ids,
        propFilter,
        (data) => {
          resolve(data);
        },
        (err) => {
          rejected(err);
        }
      );
    });
  }

  public async getProperties(dbId: number) {
    return new Promise((resolve, rejected) => {
      this.viewerComponent.viewer.getProperties(
        dbId,
        (data) => {
          resolve(data);
        },
        (err) => {
          rejected(err);
        }
      );
    });
  }

  public async search(text: string, attributeNames: string) {
    return new Promise((resolve, rejected) => {
      this.viewerComponent.viewer.search(
        text,
        (data) => {
          resolve(data);
        },
        (err) => {
          rejected(err);
        },
        [attributeNames]
      );
    });
  }

  public async selectionChanged(event: SelectionChangedEventArgs) {
    console.log(event);
    console.log('selectionChanged');
    const dbIdArray = (event as any).dbIdArray;
    console.log(dbIdArray);
    // this.viewerComponent.viewer.select(part.relations)

    if (this.button2.getState() === 0 && dbIdArray.length === 1) {
      const part = this.parts.find(x => x.internalID === dbIdArray[0]);
      var colorRed = new THREE.Vector4(1, 0, 0, 1);
      part.relations.forEach(relation => {
        this.viewerComponent.viewer.setThemingColor(relation, colorRed, this.viewerComponent.viewer.model, true);
      });
      const boundingBox = this.getModifiedWorldBoundingBoxMultiple([part.internalID]);
      this.drawBox(boundingBox.min, boundingBox.max);
      setTimeout(() => {
        this.viewerComponent.viewer.overlays.removeScene('bounding-box');
        this.viewerComponent.viewer.clearThemingColors(this.viewerComponent.viewer.model);
      }, 5000);
    }
  }

  ////////////////// NOT USED ////////////////////////////
  public async getObjectTree(): Promise<Autodesk.Viewing.InstanceTree> {
    return new Promise((resolve, rejected) => {
      this.viewerComponent.viewer.getObjectTree(
        (objectTree) => {
          resolve(objectTree);
        },
        (err) => {
          rejected(err);
        }
      );
    });
  }

  public storeAllParts() {
    asyncForEach(this.viewerLeafNodes, element => {
      this.viewerComponent.viewer.model.getProperties(element, res => {
        const bb = this.getModifiedWorldBoundingBoxMultiple([element]);
        const parentID = res.properties.find(x => x.attributeName === 'parent') ? Number(res.properties.find(x => x.attributeName === 'parent').displayValue) : undefined;
        this.viewerComponent.viewer.model.getProperties(parentID, res2 =>{
          const parentName = res2.name.slice(0,res2.name.indexOf("["));
        
        this.parts.push(
          new Part(
            element.toFixed(0),
            res.properties.find(x => x.attributeName === 'parent') ? Number(res.properties.find(x => x.attributeName === 'parent').displayValue) : undefined,
            parentName,
            res.properties.find(x => x.attributeName === 'Type Name') ? res.properties.find(x => x.attributeName === 'Type Name').displayValue : undefined,
            res.properties.find(x => x.attributeName === 'Structural Material') ? res.properties.find(x => x.attributeName === 'Structural Material').displayValue : undefined,
            1,
            1,
            Math.round(res.properties.find(x => x.attributeName === 'CUT LENGTH') ? Number(res.properties.find(x => x.attributeName === 'CUT LENGTH').displayValue) : undefined),
            0,
            [],
            // [x_max, y_max, z_max, x_min, y_min, z_min] 
            [bb.max.x, bb.max.y, bb.max.z, bb.min.x, bb.min.y, bb.min.z,]
          )
        );
      }, (error) => {
        console.log(error);
      })
    });
    });
  }

  public getModifiedWorldBoundingBoxMultiple(dbId: Array<number>) {
    const instanceTree = this.viewerComponent.viewer.model.getData().instanceTree;
    const fragIds = [];
    dbId.forEach(dbid => {
      instanceTree.enumNodeFragments(dbid, (fragId) => {
        fragIds.push(fragId);
      });
    });


    // fragments list array
    const fragList = this.viewerComponent.viewer.model.getFragmentList();
    const fragbBox = new THREE.Box3();
    const nodebBox = new THREE.Box3();

    fragIds.forEach((fragId) => {
      // @ts-ignore
      fragList.getWorldBounds(fragId, fragbBox);
      nodebBox.union(fragbBox);
    });
    return nodebBox;
  }

  public calculateNeighbors() {
    this.parts.forEach(part => {
      this.parts.forEach(partCounter => {
        // [x_max, y_max, z_max, x_min, y_min, z_min] 
        // if ((min_x_1 <= max_x_2 && max_x_1 >= min_x_2) && (min_y_1 <= max_y_2 && max_y_1 >= min_y_2) &&
        //     //                        (min_z_1 <= max_z_2 && max_z_1 >= min_z_2))
        if ((part.boundingBox[3] <= partCounter.boundingBox[0] && part.boundingBox[0] >= partCounter.boundingBox[3]) &&
          (part.boundingBox[4] <= partCounter.boundingBox[1] && part.boundingBox[1] >= partCounter.boundingBox[4]) &&
          (part.boundingBox[5] <= partCounter.boundingBox[2] && part.boundingBox[2] >= partCounter.boundingBox[5])) {
          if (part.internalID !== partCounter.internalID) {
            part.relations.push(partCounter.internalID);
            if (!this.relations.find(x => JSON.stringify(x) === JSON.stringify([partCounter.internalID, part.internalID]))) {
              this.relations.push([part.internalID, partCounter.internalID])
            }
          }
        }
      });
    });
    // 
    //                               Dann sind sie Nachbarn
  }

  ////////////////// NOT USED ////////////////////////////
  public getLeafFragIds(model, leafId) {
    const instanceTree = model.getData().instanceTree;
    const fragIds = [];

    instanceTree.enumNodeFragments(leafId, function (fragId) {
      fragIds.push(fragId);
    });

    return fragIds;
  }

  ////////////////// NOT USED ////////////////////////////
  public getComponentGeometry(dbId) {
    const viewer = this.viewerComponent.viewer;
    const fragIds = this.getLeafFragIds(viewer.model, dbId);

    let matrixWorld = null;

    const meshes = fragIds.map(function (fragId) {
      const renderProxy = viewer.impl.getRenderProxy(viewer.model, fragId);

      const geometry = renderProxy.geometry;
      const attributes = geometry.attributes;
      const positions = geometry.vb ? geometry.vb : attributes.position.array;

      const indices = attributes.index.array || geometry.ib;
      const stride = geometry.vb ? geometry.vbstride : 3;
      const offsets = geometry.offsets;

      matrixWorld = matrixWorld || renderProxy.matrixWorld.elements;

      return {
        positions,
        indices,
        offsets,
        stride,
      };
    });

    return {
      matrixWorld,
      meshes,
    };
  }

  public storeAllLeafNodes() {
    this.cbCount = 0; // count pending callbacks
    this.viewerLeafNodes = []; // store the results
    const that = this;

    return this.viewerComponent.viewer.getObjectTree(objectTree => {
      const modelRootId = objectTree.getRootId()
      return that.getLeafNodesRec(objectTree, objectTree.getRootId(), modelRootId);
    });
  }
  
  public getParentNode(node){
    return this.viewerComponent.viewer.getObjectTree(objectTree => {
      return objectTree.getNodeParentId(node)
    });
  }

  public getLeafNodesRec(objectTree, node, modelRootId) {
    this.cbCount++;
    const that = this;

    if (objectTree.getChildCount(node) !== 0) {
      objectTree.enumNodeChildren(node, (children) => {
        that.getLeafNodesRec(objectTree, children, modelRootId);
      }, false);
    } else {
      // Es muss wircklich ein Leaf Node sein. Nicht zum Beispiel ein Floor
      if (objectTree.getNodeParentId(node) !== modelRootId) {
        this.viewerLeafNodes.push(node);
      }
    }
    if (--this.cbCount === 0) {
      return true;
    }
  }

  public drawBox(min, max) {
    const linesMaterial = new THREE.LineBasicMaterial({
      color: new THREE.Color(0xFF0000),
      transparent: true,
      depthWrite: false,
      depthTest: true,
      linewidth: 10,
      opacity: 1.0
    });
    this.drawLines([

      { x: min.x, y: min.y, z: min.z },
      { x: max.x, y: min.y, z: min.z },

      { x: max.x, y: min.y, z: min.z },
      { x: max.x, y: min.y, z: max.z },

      { x: max.x, y: min.y, z: max.z },
      { x: min.x, y: min.y, z: max.z },

      { x: min.x, y: min.y, z: max.z },
      { x: min.x, y: min.y, z: min.z },

      { x: min.x, y: max.y, z: max.z },
      { x: max.x, y: max.y, z: max.z },

      { x: max.x, y: max.y, z: max.z },
      { x: max.x, y: max.y, z: min.z },

      { x: max.x, y: max.y, z: min.z },
      { x: min.x, y: max.y, z: min.z },

      { x: min.x, y: max.y, z: min.z },
      { x: min.x, y: max.y, z: max.z },

      { x: min.x, y: min.y, z: min.z },
      { x: min.x, y: max.y, z: min.z },

      { x: max.x, y: min.y, z: min.z },
      { x: max.x, y: max.y, z: min.z },

      { x: max.x, y: min.y, z: max.z },
      { x: max.x, y: max.y, z: max.z },

      { x: min.x, y: min.y, z: max.z },
      { x: min.x, y: max.y, z: max.z }],

      linesMaterial);
  }

  public drawLines(coordsArray, material) {
    for (let i = 0; i < coordsArray.length; i += 2) {
      const start = coordsArray[i];
      const end = coordsArray[i + 1];
      const geometry = new THREE.Geometry();
      geometry.vertices.push(new THREE.Vector3(start.x, start.y, start.z));
      geometry.vertices.push(new THREE.Vector3(end.x, end.y, end.z));
      geometry.computeLineDistances();
      const lines = new THREE.Line(geometry,
        material,
        THREE.LinePieces);
      if (!this.viewerComponent.viewer.overlays.hasScene('bounding-box')) {
        this.viewerComponent.viewer.overlays.addScene('bounding-box');
      }
      this.viewerComponent.viewer.overlays.addMesh(lines, 'bounding-box');
    }
  }
}
