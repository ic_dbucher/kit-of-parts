import { AuthToken } from 'forge-apis';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Part } from '../platform/models/part';

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) { }
  /* Nur für Development */
  private API_BASE = 'http://localhost:3333/api';
  // private API_BASE = 'https://felix-platform-backend.herokuapp.com';

  public async createPart(part: Part): Promise<Part> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    const partJSON = { part: part };
    return this.http.post(`${this.API_BASE}/part`, partJSON, httpOptions).toPromise()
      .then(
        res => {
          // @ts-ignore
          const returnObject = new Part(
            // @ts-ignore
            res.part.internalID,
            // @ts-ignore
            res.part.parentID,
            // @ts-ignore
            res.part.parentName,
            // @ts-ignore
            res.part.typeName,
            // @ts-ignore
            res.part.category,
            // @ts-ignore
            res.part.subCategory,
            // @ts-ignore
            res.part.length,
            // @ts-ignore
            res.part.sequenzing,
            // @ts-ignore
            res.part.relations,
          );
          return Promise.resolve(returnObject);
        })
      .catch(error => {
        // this.alertService.error(error);
        return error;
      });
  }

  public async createMultipleParts(parts: Part[]): Promise<Part[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    // const partJSON = { part: part };
    return this.http.post(`${this.API_BASE}/parts`, parts, httpOptions).toPromise()
      .then(
        res => {
          const returnArray = new Array();
          // console.log(res);         
          // @ts-ignore
          res.parts.forEach(element => {
            const temp = new Part(
              // @ts-ignore
              element.part.internalID,
              // @ts-ignore
              element.part.parentID,
              // @ts-ignore
              element.part.parentName,
              // @ts-ignore
              element.part.typeName,
              // @ts-ignore
              element.part.structuralMaterial,
              // @ts-ignore
              element.part.category,
              // @ts-ignore
              element.part.subCategory,
              // @ts-ignore
              element.part.length,
              // @ts-ignore
              element.part.sequenzing,
              // @ts-ignore
              element.part.relations,
              // @ts-ignore
              element.part.boundingBox,
            );
            returnArray.push(temp);
          });
          return Promise.resolve(returnArray);
        })
      .catch(error => {
        // this.alertService.error(error);
        return error;
      });
  }

  public async createRelations(relations: number[][]): Promise<boolean> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return await asyncForEach(relations, async (relation) => {
      const relationJSON = {
        "relation": {
          "node1": relation[0],
          "node2": relation[1],
        }
      }
      return await this.http.post(`${this.API_BASE}/relations`, relationJSON, httpOptions).toPromise()
        .then(
          res => {
            return Promise.resolve(res);
          })
        .catch(error => {
          // this.alertService.error(error);
          return error;
        });
    }).then(res => {
      return Promise.resolve(true);
    })
  }

  public async emptydatabase(): Promise<boolean> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(`${this.API_BASE}/emptydatabase`, null, httpOptions).toPromise()
      .then(
        res => {
          return Promise.resolve(res);
        })
      .catch(error => {
        // this.alertService.error(error);
        return error;
      });
  }

  public async get2LToken(): Promise<AuthToken> {
    return await this.http
      .get(`${this.API_BASE}/forge/Get2LToken`)
      .toPromise()
      .then((res) => {
        return Promise.resolve(res);
      })
      .catch((error) => {
        return error;
      });
  }

  public async getBuckets(): Promise<any> {
    return await this.http
      .get(`${this.API_BASE}/forge/getBuckets`)
      .toPromise()
      .then((res) => {
        return Promise.resolve(res);
      })
      .catch((error) => {
        return error;
      });
  }

  public async getObjects(bucketKey: string): Promise<any> {
    return await this.http
      .get(`${this.API_BASE}/forge/getObjects/${bucketKey}`)
      .toPromise()
      .then((res) => {
        return Promise.resolve(res);
      })
      .catch((error) => {
        return error;
      });
  }

  public async getspecificProject(projectID: string): Promise<any> {
    return this.http
      .get(`${this.API_BASE}/projects/getspecificProject/${projectID}`)
      .toPromise()
      .then((res) => {
        return Promise.resolve(res);
      })
      .catch((error) => {
        return error;
      });
  }
}
