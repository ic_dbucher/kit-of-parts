
import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { FORGE_CLIENT_ID, FORGE_CLIENT_SECRET } from './config';

import {
    AuthClientTwoLegged, AuthToken, ObjectsApi, BucketsApi, PostBucketsPayload,
    DerivativesApi, JobPayloadInput, JobPayloadItem, JobPayloadOutput, JobPayload, ApiResponse,
    Buckets, Bucket
} from 'forge-apis';
// import { ProjectsService } from '../projects/projects.service';
// import { Project } from 'src/types/project';
// import { UserService } from '../shared/user.service';

@Injectable()
export class ForgeService {

    // constructor(private projectsService: ProjectsService) { }
    constructor() { }

    async get2LToken(): Promise<AuthToken> {
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'data:read',
            'data:write',
        ], true);
        try {
            const authToken: AuthToken = await apiInstance.authenticate();
            return authToken;
        } catch (error) {
            return error
        }
    }

    async getBuckets(): Promise<Buckets> {
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'bucket:read',
        ], true);
        try {
            const bucketsApi: BucketsApi = new BucketsApi();
            const queryParams = {
				'region': 'US',
                'limit': 100,
                // 'startAt': opts.startAt
			};
            return await apiInstance.authenticate().then(async (res) => {
                return await bucketsApi.getBuckets(queryParams, apiInstance, res).then((buckets) => {
                    return buckets;
                });
            });
        } catch (error) {
            return error
        }
    }

    async getObjects(bucketKey: string): Promise<Bucket> {
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'data:read',
        ], true);
        try {
            const bucketsApi: ObjectsApi = new ObjectsApi();
            const queryParams = {
                'limit': 100,
                // 'beginsWith': opts.beginsWith,
				// 'startAt': opts.startAt
			};
            return await apiInstance.authenticate().then(async (res) => {
                return await bucketsApi.getObjects(bucketKey, queryParams, apiInstance, res).then((objects) => {
                    return objects;
                });
            });
        } catch (error) {
            return error
        }
    }

    // async uploadModel(input): Promise<Project> {
    //     const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
    //         'data:read',
    //         'data:write',
    //         'bucket:create',
    //     ], true);
    //     try {
    //         const postBucketsPayload: PostBucketsPayload = {
    //             bucketKey: 'felix_' + input.ProjectName + '_' + input.ProjectID, policyKey: 'persistent',
    //         };
    //         const bucketsApi: BucketsApi = new BucketsApi();
    //         const objectsApi: ObjectsApi = new ObjectsApi();
    //         await apiInstance.authenticate().then(async (res) => {

    //             await bucketsApi.createBucket(postBucketsPayload, { xAdsRegion: 'EMEA' }, apiInstance, res).then(async (resCreateBucket) => {
    //                 Logger.warn(resCreateBucket);
    //                 // if (input.Model.size > 100000) {
    //                 //     const iterations = Math.floor(input.Model.size / 5000);
    //                 //     bytes=0-32568
    //                 //     for (let index = 0; index < iterations; index++) {
    //                 //         await objectsApi.uploadChunk(resCreateBucket.body.bucketKey, input.Model.originalname,
    //                 //             5 * 1024 * 1024, input.Model.buffer, {}, apiInstance, res).then(async (resUploadObject) => {

    //                 //             });
    //                 //     }  
    //                 //     await this.projectsService.addModel(input.ProjectID, Buffer.from(resUploadObject.body.objectId).toString('base64'), resUploadObject.body.objectId);
    //                 // }
    //                 // else {
    //                 await objectsApi.uploadObject(resCreateBucket.body.bucketKey, input.Model.originalname,
    //                     input.Model.size, input.Model.buffer, {}, apiInstance, res).then(async (resUploadObject) => {
    //                         // Logger.warn(resUploadObject);
    //                         // Buffer.from('Hello World!').toString('base64') => Encode the String
    //                         // Buffer.from(b64Encoded, 'base64').toString() => Decode the String
    //                         // tslint:disable-next-line: max-line-length
    //                         await this.projectsService.addModel(input.ProjectID, Buffer.from(resUploadObject.body.objectId).toString('base64'), resUploadObject.body.objectId);
    //                     });
    //                 // }
    //             });
    //         });
    //         return await this.projectsService.getspecificProject(input.ProjectID);
    //     } catch (error) {
    //         throw new HttpException(error.statusBody.reason, HttpStatus.SEE_OTHER);
    //     }
    // }
    ////////////////// Return of objectsApi.uploadObject //////////////////
    // body
    // bucketKey: "felix_test_1_5de92fd01ee7ff63e4873988"
    // contentType: "application/octet-stream"
    // location: "https://developer.api.autodesk.com/oss/v2/buckets/felix_test_1_5de92fd01ee7ff63e4873988/objects/seven-story-concrete-tower.ifc"
    // objectId: "urn:adsk.objects:os.object:felix_test_1_5de92fd01ee7ff63e4873988/seven-story-concrete-tower.ifc"
    // objectKey: "seven-story-concrete-tower.ifc"
    // sha1:

    async startTranslation(inputUrn: string): Promise<ApiResponse> {
        // export interface JobPayloadInput {
        //     urn: string;
        //     compressedUrn?: boolean;
        //     rootFilename?: string;
        // }
        const jobPayloadInput: JobPayloadInput = { urn: inputUrn };

        // export interface JobPayloadItem {
        //     type: string;
        //     views?: string[];
        //     advanced?: JobObjOutputPayloadAdvanced;
        // }
        const jobPayloadItem: JobPayloadItem = { type: 'SVF', views: ['3d'] };

        // export interface JobPayloadOutput {
        //     formats: JobPayloadItem[];
        // }
        // const jobPayloadOutput: JobPayloadOutput = { formats: [jobPayloadItem], destination: { region: 'EMEA' } };

        // @ts-ignore
        const jobPayloadOutput: JobPayloadOutput = { formats: [jobPayloadItem], destination: { region: 'EMEA' } };

        // export interface JobPayload {
        //     input: JobPayloadInput;
        //     output: JobPayloadOutput;
        // }

        const jobPayload: JobPayload = { input: jobPayloadInput, output: jobPayloadOutput };
        // ersetzt bereits vorhande Derivatives
        const opts = { xAdsForce: true };
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'data:read',
            'data:write',
            'data:create',
        ], true);
        const derivativesApi: DerivativesApi = new DerivativesApi();
        // diagnostic:"The parameter 'output::formats::views'(type=svf) is required."

        try {
            return await apiInstance.authenticate().then(async (res) => {
                // NPM Module Path to EMEA geändert
                return await derivativesApi.translate(jobPayload, opts, apiInstance, res).then(resTranslate => {
                    return resTranslate;
                });
            });
        } catch (error) {
            throw new HttpException(error.statusBody.reason, HttpStatus.SEE_OTHER);
        }
    }

    async getTranslationStatus(inputUrn: string): Promise<ApiResponse> {
        const derivativesApi: DerivativesApi = new DerivativesApi();
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'data:read',
            'viewables:read',
        ], true);
        try {
            inputUrn = inputUrn.replace('=', '');
            inputUrn = inputUrn.replace('=', '');
            inputUrn = inputUrn.replace('=', '');

            return await apiInstance.authenticate().then(async (res) => {
                // NPM Module Path to EMEA geändert
                return await derivativesApi.getManifest(inputUrn, { acceptEncoding: '' }, apiInstance, res).then((resGetManifest) => {
                    return resGetManifest.body;
                });
            });
        } catch (error) {
            throw new HttpException(error.statusBody.reason, HttpStatus.SEE_OTHER);
        }
    }

    async deleteBucket(projectID: string, projectName: string): Promise<any> {
        const apiInstance = new AuthClientTwoLegged(FORGE_CLIENT_ID, FORGE_CLIENT_SECRET, [
            'bucket:delete',
        ], true);
        try {
            // bucketKey: 'felix_' + input.ProjectName + '_' + input.ProjectID

            // BucketKey darf keine Spaces haben beim löschen
            const str = projectName.toLowerCase();
            const replaced = str.split(' ').join('_');
            const bucketKey: string = ('felix_' + replaced + '_' + projectID);
            // Hier noch überprüfen
            // bucketKey = encodeURI(bucketKey);
            const bucketsApi: BucketsApi = new BucketsApi();
            return await apiInstance.authenticate().then(async (res) => {
                return await bucketsApi.deleteBucket(bucketKey, apiInstance, res).then((resDeleteBucket) => {
                    // Logger.warn(resDeleteBucket);
                    return resDeleteBucket;
                });
            });
        } catch (error) {
            throw new HttpException(error.statusBody.reason, HttpStatus.SEE_OTHER);
        }
    }
}
