import { Controller, Get, UseGuards, Post, UseInterceptors, UploadedFile, Param, Delete } from '@nestjs/common';
import { ForgeService } from './forge.service';
import { AuthToken, Buckets } from 'forge-apis';
import {  } from "module";
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('forge')
export class ForgeController {
    constructor(private forgeService: ForgeService) { }

    @Get('Get2LToken')
    async get2LToken() {
        const authToken: AuthToken = await this.forgeService.get2LToken();
        return authToken;
    }

    @Get('getBuckets')
    async getBuckets() {
        const buckets: Buckets = await this.forgeService.getBuckets();
        return buckets;
    }

    @Get('getObjects/:bucketKey')
    async getObjects(@Param('bucketKey') bucketKey) {
        const buckets: Buckets = await this.forgeService.getObjects(bucketKey);
        return buckets;
    }


    // @Post('UploadModel/:projectID/:projectName')
    // @UseInterceptors(FileInterceptor('model'))
    // async uploadModel(@UploadedFile() model, @Param('projectID') projectID, @Param('projectName') projectName) {
    //     const temp = projectName.split(' ');
    //     const modifyedProjectName = temp.join('_').toLowerCase();
    //     const input = {
    //         Model: model,
    //         Originalname: model.originalname,
    //         ProjectID: projectID,
    //         ProjectName: modifyedProjectName,
    //     };
    //     const res = await this.forgeService.uploadModel(input);
    //     return res;
    // }

    @Get('startTranslation/:urn')
    async startTranslation(@Param('urn') modelUrn) {
        const res = await this.forgeService.startTranslation(modelUrn);
        return res;
    }

    @Get('getTranslationStatus/:urn')
    async getTranslationStatus(@Param('urn') modelUrn) {
        const res = await this.forgeService.getTranslationStatus(modelUrn);
        return res;
    }

    @Delete('deleteBucket/:projectID/:projectName')
    async deleteBucket(@Param('projectID') projectID, @Param('projectName') projectName) {
        const res = await this.forgeService.deleteBucket(projectID, projectName);
        return res;
    }
}
