import { Injectable, Inject } from '@nestjs/common';

import { Message } from '@myorg/api-interfaces';
import { Neo4jService } from 'nest-neo4j/dist';
import { Part } from './entity/part.entity';
import { CreatePartDto } from './dto/create-part.dto';

@Injectable()
export class AppService {

  constructor(private readonly neo4jService: Neo4jService) { }

  getHello(): Message {
    return { message: 'Welcome to api!' };
  }

  createPart(internalID: number, parentID: number, parentName: string, typeName: string, structuralMaterial:string, category: number, subCategory: number, length: number, sequenzing: number, relations: number[], boundingBox: number[]): Promise<Part> {
    return this.neo4jService.write(
      `  
        CREATE (a:Part)
        SET a += $part
        RETURN a
    `, {
      part: { internalID, parentID, parentName, typeName, structuralMaterial, category, subCategory, length, sequenzing, relations, boundingBox },
    })
      .then(res => {
        const row = res.records[0]
        return new Part(
          row.get('a')
        )
      })
  }

  createMultipleParts(parts: CreatePartDto[]): Promise<Part[]> {
    return this.neo4jService.write(
      `  
        UNWIND $props AS map
        CREATE (a:Part)
        SET a = map
        RETURN *
    `, {
      props: parts,
    })
      .then(res => {
        const returnArray = new Array();
        res.records.forEach(element => {
          const part = new Part(element.get('a').properties)
          returnArray.push(part)
        });
        return returnArray
      })
  }

  createRelations(node1: number, node2: number): Promise<any> {
    return this.neo4jService.write(
      `  
        MATCH ( nd1 :Part {internalID: $node1}), (nd2 :Part {internalID: $node2} )
        CREATE (nd1)-[r :RELATED {testing: 'testing'}]->(nd2)
        RETURN r
    `, {
      node1: node1,
      node2: node2
    })
      .then(res => {
        return true;
        // const row = res.records[0]
        // return res
        // return new Part(
        //   row.get('r')
        // )
      })
  }

  emptydatabase(): Promise<boolean> {
    return this.neo4jService.write(
      `
        MATCH (n)
        DETACH DELETE n
    `, {})
      .then(res => {
        return true;
      })
  }
}
