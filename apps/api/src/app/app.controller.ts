import { Controller, Get, Post, Body } from '@nestjs/common';

import { Message } from '@myorg/api-interfaces';

import { AppService } from './app.service';

import { ConfigService } from '@nestjs/config';

import { Neo4jService } from 'nest-neo4j/dist';
import { CreatePartDto } from './dto/create-part.dto';



@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly neo4jService: Neo4jService,
  ) { }

  @Get('hello')
  getData(): Message {
    return this.appService.getHello();
  }

  @Get()
  async getHello(): Promise<string> {
    // const greeting = await this.appService.getHello()
    // return greeting
    const res = await this.neo4jService.read(`MATCH (n) RETURN count(n) AS count`)
    return `There are ${res.records[0].get('count')} nodes in the database`
  }

  @Post('part')
  async createPart(@Body() createPartDto: CreatePartDto) {
    const part = await this.appService.createPart(
      createPartDto.part.internalID,
      createPartDto.part.parentID,
      createPartDto.part.parentName,
      createPartDto.part.typeName,
      createPartDto.part.structuralMaterial,
      createPartDto.part.category,
      createPartDto.part.subCategory,
      createPartDto.part.length,
      createPartDto.part.sequenzing,
      createPartDto.part.relations,
      createPartDto.part.boundingBox,
    )
    return {
      part: part.toJson()
    }
  }

  @Post('parts')
  async createMultipleParts(@Body() createPartDto: CreatePartDto[]) {
    const parts = await this.appService.createMultipleParts(createPartDto)
    return {
      parts
      // parts: parts.toJson()
      // part
      // // part: part.toJson()
    }
  }

  @Post('relations')
  async createRelations(@Body() relationDto: any) {
    const relation = await this.appService.createRelations(
      relationDto.relation.node1,
      relationDto.relation.node2,
    )
    return {
      relation
      // part: relation.toJson()
    }
  }

  @Post('emptydatabase')
  async emptydatabase() {
    const result = await this.appService.emptydatabase()
    return {
      result
    }
  }
}