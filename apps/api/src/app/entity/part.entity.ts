import { Node } from 'neo4j-driver'

export class Part {
    constructor(
        private readonly part: Node,
    ) {}

    toJson(): Record<string, any> {
        return {
            ...this.part.properties,
        }
    }
}