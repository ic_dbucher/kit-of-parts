// {"part":{"category":"Here comes a numer -> 1, 1.1 etc", "length":"Here comes a numer -> 1, 1.1 etc"}}

import { IsNotEmpty, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

class Part {

    @IsNotEmpty()
    internalID: number;

    @IsNotEmpty()
    parentID: number;

    @IsNotEmpty()
    parentName: string;

    @IsNotEmpty()
    typeName: string;

    @IsNotEmpty()
    structuralMaterial: string;

    @IsNotEmpty()
    category: number;

    @IsNotEmpty()
    subCategory: number;

    @IsNotEmpty()
    length: number;

    @IsNotEmpty()
    sequenzing: number;

    @IsNotEmpty()
    relations: number[];

    @IsNotEmpty()
    boundingBox: number[];
}

export class CreatePartDto {

    @ValidateNested()
    @Type(() => Part)
    part: Part;

}